import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // create an array list for dataSet
        ArrayList<Integer[]> dataSet = new ArrayList<>();
        ArrayList<Double> averages = new ArrayList<>();

        // asks for how many cases
        System.out.println("Test cases:");
        int testCases = Integer.parseInt(scan.nextLine());

        for (int i = 0; i < testCases; i++) {
            // ask for and get number of people
            System.out.println("People in the case " + (i + 1) + ":");
            // how many people in each case
            int people = Integer.parseInt(scan.nextLine());

            // create an array for each case
            Integer[] currentCase = new Integer[people + 1];
            // add first element to represent amount of people
            currentCase[0] = people;
            // add current case into the ArrayList cases
            dataSet.add(currentCase);
            // asking for the grades
            System.out.println("Enter grades:");

            for (int j = 1; j < currentCase.length; j++) {
                // scan given grade
                int grade = Integer.parseInt(scan.nextLine());

                if (grade >= 0 && grade <= 100) {
                    currentCase[j] = grade;
                }
            }

            averages.add(averageOfCase(currentCase));
        }

        printAverages(averages);
    }

    public static double averageOfCase(Integer[] currCase) {
        int sum = 0;
        for (int i = 1; i < currCase.length; i++) {
            sum += currCase[i];
        }

        return 1.00d * sum / currCase[0];
    }

    public static double getPercentage(int totalStudents, int studentsAboveAVG) {
        return 1.0 * studentsAboveAVG * 100 / totalStudents;
    }

    public static void printAverages(ArrayList<Double> avgs) {
        DecimalFormat decimal = new DecimalFormat("#.000");

        for (Double avg: avgs) {
            System.out.println(decimal.format(avg) + "%");
        }
    }
}
